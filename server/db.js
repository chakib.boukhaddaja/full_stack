const mongoose = require("mongoose");

module.exports = () => {
	const connectionParams = {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	};
	try {
		mongoose.connect(`mongodb://localhost:27017/devopsDemo`,connectionParams);
		// mongoose.connect('mongodb+srv://chakib:chakib@cluster0.oprlt.mongodb.net/devopsDemo?retryWrites=true&w=majority',connectionParams);
		console.log("Connected to database successfully");
	} catch (error) {
		console.log(error);
		console.log("Could not connect to the database!");
	}
};
