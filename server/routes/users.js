const router = require("express").Router();
const { User, validate } = require("../models/user");
const bcrypt = require("bcrypt");

router.get('/', (req,res) =>{
	res.send("Hello World");
})

router.post("/", async (req, res) => {
	try {
		console.log("hel");
		const { error } = validate(req.body);
		if (error){
			return res.status(400).send({ message: error.details[0].message });
		}

		console.log("hell");
		const user = await User.findOne({ email: req.body.email });
		if (user){
			return res
				.status(409)
				.send({ message: "User with given email already Exist!" });
		}

		console.log("hello");
		const salt = await bcrypt.genSalt(Number(10));
		const hashPassword = await bcrypt.hash(req.body.password, salt);
		console.log("hellopp");
		await new User({ ...req.body, password: hashPassword }).save();
		res.status(201).send({ message: "User created successfully" });
	} catch (error) {
		res.status(500).send({ message: "Internal Server Error" });
	}
});

module.exports = router;
